import * as React from 'react';
import { View, Text, Dimensions, ImageBackground, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Neomorph, NeomorphFlex } from 'react-native-neomorph-shadows';
import Carousel from 'react-native-snap-carousel';
import { Pagination } from 'react-native-snap-carousel';
import Arrow from './assets/arrow.svg'
function Intro() {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const [activeSlide, setActiveSlide] = React.useState(0)
    const entries = [
        {
            image: "https://images.unsplash.com/photo-1445384763658-0400939829cd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
            title: "Enim id ullamco ipsum cillum ad dolor ea.",
            caption: "Nostrud commodo ullamco ullamco tempor proident ad culpa eiusmod nisi cillum enim."
        },
        {
            image: "https://images.unsplash.com/photo-1500468756762-a401b6f17b46?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=688&q=80",
            title: "Mollit excepteur qui pariatur labore commodo veniam mollit.",
            caption: "Laboris exercitation reprehenderit est quis aliquip est eu aliquip eu."
        },
        {
            image: "https://images.unsplash.com/photo-1626026397008-3316047db4fc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=943&q=80",
            title: "Consectetur occaecat enim et id aute reprehenderit sit.",
            caption: "Occaecat velit ut reprehenderit qui dolore irure et ea culpa eu."
        }
    ]

    const styles = StyleSheet.create({
        hero: {
            fontSize: 20,
            color: '#000',
            fontWeight: '600',
            marginTop: 20,
            marginBottom: 10
        },
        button: {
            width: 50,
            height: 50,
            backgroundColor: '#6666cc',
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center',
        },
        neuromporphFlex: {
            width: windowWidth,
            shadowRadius: 10,
            borderTopRightRadius: 25,
            bottom: 0,
            position: 'absolute',
            paddingBottom: 30,
            backgroundColor: 'white'
        },
        neuromporphFlexWrapper: {
            height: windowHeight
        },
        textWrapper: {
            paddingHorizontal: 20,
            paddingRight: 90,
            paddingBottom: 50,
        },
        paginationContainer: {
            alignContent: 'flex-start',
            position: 'absolute',
            bottom: 0,
            marginLeft: 0,
        },
        buttonWrapper: {
            position: 'absolute',
            right: 20,
            bottom: 20,
        }
    })
    let carouselRef = React.useRef()

    const _renderItem = ({ item, index }) => {
        return (
            <View>
                <ImageBackground source={{ uri: item.image }} resizeMode="cover">
                    <View style={styles.neuromporphFlexWrapper}>
                        <NeomorphFlex style={styles.neuromporphFlex}>
                            <View>
                                <View style={styles.textWrapper}>
                                    <Text style={styles.hero}>{item.title}</Text>
                                    <Text adjustsFontSizeToFit numberOfLines={6} style={{ textAlign: 'justify' }}>{item.caption}</Text>
                                </View>
                                <View style={{ marginTop: 30 }}>
                                    <Pagination
                                        containerStyle={styles.paginationContainer}
                                        dotsLength={entries.length}
                                        activeDotIndex={activeSlide}
                                        dotStyle={{
                                            backgroundColor: '#6666cc'
                                        }}
                                        inactiveDotOpacity={0.4}
                                        inactiveDotScale={0.6}
                                    />
                                </View>
                                <View style={styles.buttonWrapper}>
                                    <TouchableOpacity style={styles.button} onPress={() => {
                                        if (activeSlide === (entries.length - 1)) {
                                            return Alert.alert('Completed')
                                        }
                                        carouselRef.snapToNext()
                                    }}>
                                        <Arrow width={25} height={25} color='#FFF' />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </NeomorphFlex>

                    </View>

                </ImageBackground>

            </View>
        );
    }

    return (
        <Carousel
            ref={(c) => { carouselRef = c; }}
            data={entries}
            renderItem={_renderItem}
            sliderWidth={windowWidth}
            sliderHeight={1000000}
            itemWidth={windowWidth}
            itemHeight={1000000}
            onSnapToItem={(index) => setActiveSlide(index)}
        />

    );
}
export default Intro
